import {Fragment} from 'react'
import AppNavbar from './../components/AppNavbar';
import Banner from './../components/Banner';
import Footer from './../components/Footer';

export default function Home() {
	return (
		<Fragment>
			<AppNavbar/>
			<Banner/>
			<Footer/>
		</Fragment>
		)
}