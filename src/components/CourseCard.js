

export default function CourseCard(){
	return(
		<Card style={{ width: '18rem' }}>
  <Card.Img variant="top" src="holder.js/100px180" />
  <Card.Body>
    <Card.Title>Sample Course</Card.Title>
    <Card.Text>
     Description: This is a sample course offering.
    </Card.Text>
    <Card.Text>
     Price:  PhP 40,000
    </Card.Text>
    <Button variant="primary">Enroll</Button>
  </Card.Body>
</Card>
		)
}