
import {Navbar, Container, Nav} from 'React-Bootstrap'

export default function appNavBar(){
	return(
		    <Navbar bg="info" expand="lg">
		  <Container>
		    <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="me-auto">
		        <Nav.Link href="#home">Home</Nav.Link>
		        <Nav.Link href="#link">Courses</Nav.Link>
		        <NavDropdown title="Dropdown" id="basic-nav-dropdown">
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
		)
}

